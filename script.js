var app = new Vue({
    el: '#app',
    data: {
        users: [
            {
                img: 'img.jpg',
                name: 'John Adams',
                rating: 102,
                ideas: 0,
                following: 50,
                followers: 0,
                addIcon: true,
                followBtn: false,
                followedIcon: false,
                unfollowBtn: false
            },
            {
                img: 'img.jpg',
                name: 'Kevin Malcolm',
                rating: 1472,
                ideas: 20,
                following: 19,
                followers: 26,
                addIcon: true,
                followBtn: false,
                followedIcon: false,
                unfollowBtn: false
            },
            {
                img: 'img.jpg',
                name: 'James Madison',
                rating: 32,
                ideas: 0,
                following: 2,
                followers: 0,
                addIcon: true,
                followBtn: false,
                followedIcon: false,
                unfollowBtn: false
            },
            {
                img: 'img.jpg',
                name: 'James Manroe',
                rating: 3086,
                ideas: 53,
                following: 1059,
                followers: 45,
                addIcon: true,
                followBtn: false,
                followedIcon: false,
                unfollowBtn: false
            },
            {
                img: 'img.jpg',
                name: 'Andrew Jackson',
                rating: 102,
                ideas: 0,
                following: 50,
                followers: 0,
                addIcon: true,
                followBtn: false,
                followedIcon: false,
                unfollowBtn: false
            }
        ]
    },
    methods: {
        showAdd(user) {
            var index = this.users.indexOf(user);

            this.users[index].addIcon = true;
            this.users[index].followBtn = false;
            this.users[index].followedIcon = false;
            this.users[index].unfollowBtn = false;
        },
        showFollowBtn(user) {
            var index = this.users.indexOf(user);

            this.users[index].addIcon = false;
            this.users[index].followBtn = true;
            this.users[index].followedIcon = false;
            this.users[index].unfollowBtn = false;
        },
        follow(user) {
            var index = this.users.indexOf(user);

            this.users[index].addIcon = false;
            this.users[index].followBtn = false;
            this.users[index].followedIcon = true;
            this.users[index].unfollowBtn = false;
        },
        showUnfollowBtn(user) {
            var index = this.users.indexOf(user);

            this.users[index].addIcon = false;
            this.users[index].followBtn = false;
            this.users[index].followedIcon = false;
            this.users[index].unfollowBtn = true;
        }
    }
});
